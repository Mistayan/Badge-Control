from abc import ABC, abstractmethod


class ILecteurBadge(ABC):
    """
    Interface lecteur de badge
    """

    @abstractmethod
    def detecte_badge(self, badge) -> str:
        """
        Lecture du badge
        """
        pass

    @property
    @abstractmethod
    def signal_ouverture(self) -> bool:
        """
        Le signal d'ouverture est actif si le badge est actif et que le timer n'est pas dépassé.
        :return: True si le signal d'ouverture est actif, False sinon
        """
        pass

    @abstractmethod
    def add_callback(self, callback):
        """
        Ajout d'un callback au lecteur de badge.
        :param callback:
        :return:
        """
        pass

    def detect_badge(self, badge):
        """
        Lecture d'un badge en cours. (composant PASSIF)
        :param badge:
        :return:
        """
        pass
