from abc import ABC, abstractmethod

from src.models.custom_error import CustomError
from src.models.ifaces.interface_porte import IPorte


class IMotor(ABC):

    @abstractmethod
    def __init__(self, lecteurs_badges, porte, control_center):
        from src.models.centre_de_control import CentreDeControl
        if not isinstance(control_center, CentreDeControl):
            raise CustomError("Le centre de contrôle est requis")
        if not isinstance(porte, IPorte):
            raise CustomError("La porte est requise")
        if not isinstance(lecteurs_badges, list) or len(lecteurs_badges) == 0:
            raise CustomError("Les lecteurs de badges sont requis")

    @property
    @abstractmethod
    def id(self):
        pass
