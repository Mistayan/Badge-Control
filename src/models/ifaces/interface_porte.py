"""
Une porte sécurisée est un objet qui peut être ouvert ou fermé par un utilisateur autorisé.
Elle possède un verrou, qui peut être actionné par un moteur.
Le moteur est électriquement relié à un lecteur de badge, qui peut être activé par un badge valide.
"""

from abc import ABC, abstractmethod


class IPorte(ABC):

    @abstractmethod
    def __init__(self):
        pass

    @property
    @abstractmethod
    def ouverte(self):
        pass
