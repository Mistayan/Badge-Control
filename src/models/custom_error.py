import logging


class CustomError(Exception):
    def __init__(self, message):
        logging.error(message)
        super().__init__(message)
