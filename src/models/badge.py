import random


class Badge:
    """
    Un badge est un objet qui permet d'autoriser l'accès à une porte.
    Il se défini par un identifiant unique et un statut activé/désactivé.
    """

    def __init__(self):
        self.__id = random.randint(0, 1000)
        self.__actif: bool = True

    def __repr__(self):
        return "<Badge(id_badge='%s', actif='%s')>" % (
            self.__id, self.__actif)

    def set_actif(self, actif: bool):
        self.__actif = actif

    @property
    def actif(self):
        return self.__actif

    @property
    def id_badge(self):
        return self.__id
