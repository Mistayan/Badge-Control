import logging
import random

from src.models.custom_error import CustomError
from src.models.ifaces.interface_lecteur_badge import ILecteurBadge
from src.models.ifaces.interface_moteur import IMotor


class Motor(IMotor):
    """
    Un Moteur est monté sur une porte et est relié à un lecteur de badge.
    Quand le lecteur de badge est activé, il attend le scan du moteur.
    Régulièrement, le moteur vérifie si les lecteurs de badges reliés ont été activés.
    Quand un lecteur de badge est activé, le moteur vérifie si le badge est valide.
    Si le badge est valide, le moteur ouvre la porte.
    """
    def __init__(self, lecteurs_badges, porte, control_center):
        super().__init__(lecteurs_badges, porte, control_center)
        self.__id = random.randint(0, 1000)
        self._logger = logging.getLogger(self.__class__.__name__)
        # start watching __lecteur_badge.signal_ouverture
        # if the value changes, call __callback
        # This is only present to avoid dealing with async methods
        self.__porte = porte
        for lecteur in lecteurs_badges:
            if not isinstance(lecteur, ILecteurBadge):
                raise CustomError(str(lecteur) + " n'est pas un lecteur de badge")
            lecteur.add_callback(self.__callback)
        self.__lecteurs_badges = lecteurs_badges
        self.__control_center = control_center

    def __callback(self, badge_id, reader_id):
        """
        Callback du lecteur de badge.
        :param badge_id:
        """
        self._logger.info("Callback du lecteur de badge" + str(reader_id))
        if badge_id is None:
            self.__porte.signal_recu = False
            return False
        if self.__control_center.check(badge_id, self.__id):
            self._logger.info("Signal d'ouverture de la porte")
            self.__porte.signal_recu = True
            return True
        return self.__porte.signal_recu

    @property
    def id(self):
        return self.__id
