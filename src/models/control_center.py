import logging
from functools import wraps

from src.models.badge import Badge


# typing decorator to check for requested type
def type_check(wanted_types: list[type]):
    def decorator(func):
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            for i, arg in enumerate(args):
                if not isinstance(arg, wanted_types[i]):
                    raise TypeError(f"Argument {arg} is not of type {args[i]}")
            return func(self, *args, **kwargs)

        return wrapper

    return decorator


class ControlCenter:
    def __init__(self):
        self.__logger = logging.getLogger(self.__class__.__name__)
        self.__badges = set()

    @type_check([Badge])
    def check(self, badge: Badge):
        for _b in self.__badges:
            if _b.id_badge == badge.id_badge and _b.actif:
                self.__logger.info("Badge actif")
                return True
        self.__logger.info("Badge inactif")
        return False

    @type_check([Badge])
    def register(self, badge: Badge):
        if badge not in self.__badges:
            self.__badges.add(badge)
        return badge

    @type_check([Badge])
    def unblock(self, badge: Badge) -> Badge | int:
        self.__logger.info("Unblocking badge " + str(badge.id_badge))
        for _b in self.__badges:
            if _b.id_badge == badge.id_badge:
                _b.set_actif(True)
                return _b

    @type_check([Badge])
    def block(self, badge: Badge) -> Badge | int:
        self.__logger.info("Blocking badge " + str(badge.id_badge))
        for _b in self.__badges:
            if _b.id_badge == badge.id_badge:
                _b.set_actif(False)
                return _b


if __name__ == '__main__':
    cc = ControlCenter()
    b = Badge()
    logging.info(cc.register(b))
    logging.info(cc.unblock(b))
