import logging
from functools import wraps

from src.models.badge import Badge
from src.models.custom_error import CustomError
from src.models.ifaces.interface_moteur import IMotor


# typing decorator to check for requested type
def type_check(wanted_types: list[type]):
    def decorator(func):
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            for i, arg in enumerate(args):
                if not isinstance(arg, wanted_types[i]):
                    raise TypeError(f"Argument {arg} is not of type {wanted_types[i]}")
            return func(self, *args, **kwargs)

        return wrapper

    return decorator


class CentreDeControl:
    def __init__(self):
        self.__logger = logging.getLogger(self.__class__.__name__)
        self.__badges = {}
        self.__association = {}

    @type_check([int, int])
    def check(self, badge_id, motor_id):
        for _b in self.__badges:
            if _b.id_badge == badge_id and _b.actif is True and self.__check_access(_b, motor_id) is True:
                self.__logger.info("Badge OK")
                return True
        self.__logger.info("Badge NOK")
        return False

    @type_check([Badge, IMotor | None])
    def register(self, badge: Badge, allowed_motor: IMotor | None = None):
        self.__logger.info("Registering badge " + str(badge.id_badge))
        if badge not in self.__badges:
            self.__badges[badge] = []
        if allowed_motor:
            if badge not in self.__association:
                self.__association[badge] = [allowed_motor.id]
            else:
                self.__association[badge].append(allowed_motor.id)
            self.__badges[badge].append(allowed_motor.id)
        return badge

    @type_check([Badge])
    def unblock(self, badge: Badge) -> Badge:
        self.__logger.info("Unblocking badge " + str(badge.id_badge))
        for _b in self.__badges:
            if _b.id_badge == badge.id_badge:
                _b.set_actif(True)
                return _b

    @type_check([Badge])
    def block(self, badge: Badge) -> Badge:
        self.__logger.info("Blocking badge " + str(badge.id_badge))
        for _b in self.__badges:
            if _b.id_badge == badge.id_badge:
                _b.set_actif(False)
                return _b

    @type_check([Badge, int])
    def __check_access(self, badge, motor_id):
        self.__logger.info("Checking access for badge " + str(badge.id_badge) + " and door " + str(motor_id))
        doors = self.__badges[badge]
        if motor_id in doors:
            return True
        return False

    @type_check([Badge, int])
    def add_access(self, badge, motor_id):
        if badge not in self.__badges:
            raise CustomError("Badge not found")
        self.__badges[badge].append(motor_id)

    @type_check([Badge, int])
    def remove_access(self, badge, motor_id):
        if badge not in self.__badges:
            raise CustomError("Badge not found")
        self.__badges[badge].remove(motor_id)

    @property
    def badges(self):
        return self.__badges
