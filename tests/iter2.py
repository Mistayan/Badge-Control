import unittest

from src.models.badge import Badge
from src.models.centre_de_control import CentreDeControl
from src.models.custom_error import CustomError
from src.models.moteur import Motor
from .models.lecteur_badge_fake import LecteurBadgeFake
from .models.porte_spy import PorteSpy
from .mvp import configure_test_environment


class TestIter2ControlParPorte(unittest.TestCase):

    @unittest.skip("rappel")
    def test_UC1(self):
        """ ### UC1: Ouverture de porte classique (rappel)"""
        # ETANT DONNE une Porte reliée à un Lecteur de badge
        # ET un Moteur d'Ouverture interrogeant ce Lecteur
        porte, control_center, lecteur_badge, moteur = configure_test_environment(1)

        # Et un badge valide, ayant les accès requis à la porte
        badge = control_center.register(Badge(), allowed_motor=moteur)

        # Quand ce badge est présenté sur le lecteur
        lecteur_badge.detecte_badge(badge)

        # Alors le signal d'ouverture de la porte est envoyé
        self.assertTrue(porte.ouverte)

    def test_deux_portes_un_badge(self):
        """UC9: Deux portes reliées à un lecteur"""
        # ETANT DONNE un centre de controle
        control_center = CentreDeControl()

        # ET deux portes autorisée
        porte_autorisee_1, lecteur1 = PorteSpy(), LecteurBadgeFake()
        moteur1 = Motor(lecteurs_badges=[lecteur1], porte=porte_autorisee_1, control_center=control_center)

        porte_autorisee_2, lecteur2 = PorteSpy(), LecteurBadgeFake()
        moteur2 = Motor(lecteurs_badges=[lecteur2], porte=porte_autorisee_2, control_center=control_center)

        # ET une porte non autorisée
        porte_non_autorisee, lecteur3 = PorteSpy(), LecteurBadgeFake()
        moteur3 = Motor(lecteurs_badges=[lecteur3], porte=porte_non_autorisee, control_center=control_center)

        # ET un individu ayant un badge valide, avec les accès requis à la porte pour les deux portes autorisées
        badge_unique = Badge()
        control_center.register(badge_unique, allowed_motor=moteur1)
        control_center.register(badge_unique, allowed_motor=moteur2)

        autre_badge = Badge()
        control_center.register(autre_badge, allowed_motor=moteur3)

        # QUAND le badge est passe successivement devant les trois lecteurs
        lecteur1.detecte_badge(badge_unique)
        lecteur2.detecte_badge(badge_unique)
        lecteur3.detecte_badge(badge_unique)

        # ALORS les deux portes autorisées sont ouvertes avec le meme badge, et une porte non autorisée reste fermée
        self.assertTrue(porte_autorisee_1.ouverte)
        self.assertTrue(porte_autorisee_2.ouverte)
        self.assertFalse(porte_non_autorisee.ouverte)

    def test_autorisation_acces_badge_connu(self):
        """UC10: Ajout d'autorisation d'accès d'un badge connu"""
        # ETANT DONNE un centre de contrôle et un moteur
        control_center = CentreDeControl()
        motor_id = 1
        badge = Badge()
        control_center.register(badge)
        # QUAND on ajoute l'accès pour un badge connu à un moteur
        control_center.add_access(badge, motor_id)
        # ALORS l'identifiant du moteur est ajouté à la liste d'accès du badge
        self.assertIn(motor_id, control_center.badges[badge])

    def test_autorisation_acces_badge_inconnu(self):
        """UC11: Ajout d'autorisation d'accès d'un badge inconnu"""
        # ETANT DONNE un centre de contrôle et un moteur
        control_center = CentreDeControl()
        motor_id = 1
        # QUAND on tente d'autoriser l'accès à un moteur pour un badge qui n'existe pas
        # ALORS une erreur est levée
        with self.assertRaises(CustomError):
            control_center.add_access(Badge(), motor_id)

    def test_retrait_acces_badge_connu(self):
        """UC12: Retrait d'autorisation d'un badge connu"""
        # ETANT DONNE un centre de contrôle, un moteur et un accès ajouté pour un badge
        control_center = CentreDeControl()
        motor_id = 1
        badge = Badge()
        control_center.register(badge)
        control_center.add_access(badge, motor_id)
        # QUAND on supprime l'accès au moteur pour le badge connu
        control_center.remove_access(badge, motor_id)
        # Alors l'identifiant du moteur est supprimé de la liste d'accès du badge
        self.assertNotIn(motor_id, control_center.badges[badge])

    def test_retrait_acces_badge_inconnu(self):
        """UC13: Retrait d'autorisation d'un badge inconnu"""
        # ETANT DONNE un centre de contrôle et un moteur
        control_center = CentreDeControl()
        motor_id = 1
        badge = Badge()
        control_center.register(badge)
        # QUAND on supprime l'accès pour un badge qui n'existe pas
        # ALORS une erreur est levée
        with self.assertRaises(CustomError):
            control_center.remove_access(Badge(), motor_id)

    def test_informations_badge(self):
        """UC14: Recuperation d'information d'un badge"""
        # ETANT DONNE un badge
        badge = Badge()
        # QUAND on souhaite recupere des informations
        informations = str(badge)
        # ALORS l'id et le status d'activation du badge sont données
        self.assertTrue(informations.startswith('<Badge(id_badge=\''))
        self.assertTrue(informations.endswith('\', actif=\'True\')>'))

    def test_UC15(self):
        """ ### UC15: Le badge n'est pas autorisé à ouvrir la porte """
        # Etant donné une porte reliée à un lecteur de badge
        # Et un moteur d'ouverture interrogeant ce lecteur
        porte, control_center, lecteur_badge, moteur = configure_test_environment(1)

        # Et un badge n'ayant pas les accès requis à la porte
        badge = control_center.register(Badge(), allowed_motor=None)
        control_center.unblock(badge)  # On s'assure que le badge est effectivement débloqué

        # Quand ce badge est présenté sur le lecteur
        lecteur_badge.detecte_badge(badge)

        # Alors le signal d'ouverture de la porte n'est pas envoyé et la porte reste fermée
        self.assertFalse(porte.ouverte)
