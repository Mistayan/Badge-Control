import logging
import random

from src.models.badge import Badge
from src.models.centre_de_control import type_check
from src.models.ifaces.interface_lecteur_badge import ILecteurBadge


class LecteurBadgeFake(ILecteurBadge):

    def __init__(self):
        """
        Un lecteur de badge est un objet qui permet de lire un badge.
        Il se définit par un identifiant unique.
        il est relié à

        """
        super().__init__()
        self.__id = random.randint(0, 1000)
        self.__logger = logging.getLogger(self.__class__.__name__ + str(self.__id))
        self.__last_badge = None
        self.__callback = None

    @property
    def signal_ouverture(self):
        """
        Le signal d'ouverture est envoyé si le badge est validé.
        :return: True si le signal d'ouverture est actif, False sinon
        """
        self.__logger.info("Signal d'ouverture : {}".format(self.__last_badge))
        if not self.__last_badge:
            return False
        return True

    @type_check([Badge])
    def detecte_badge(self, badge):
        """
        Lecture d'un badge en cours.
        Si le badge est actif, le lecteur de badge devient actif.
        :param badge: badge à lire
        :return: None
        """
        self.__last_badge = badge.id_badge
        self.__logger.info("Lecture du badge {}".format(badge.id_badge))
        # Avoid async methods in motor class
        if self.__callback and self.__last_badge:
            self.__callback(self.__last_badge, self.__id)

    def add_callback(self, callback):
        """
        Ajout d'un callback au lecteur de badge.
        :param callback:
        :return:
        """
        self.__callback = callback
