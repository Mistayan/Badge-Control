import logging

from src.models.ifaces.interface_porte import IPorte


class PorteSpy(IPorte):

    def __init__(self):
        self.count = 0
        self.__logger = logging.getLogger(self.__class__.__name__)
        self.__signal_recu = False

    @property
    def ouverte(self):
        res = self.signal_recu is True
        self.__logger.info("Porte ouverte : {}".format(res))
        return res

    @property
    def signal_recu(self):
        return self.__signal_recu

    @signal_recu.setter
    def signal_recu(self, value):
        self.count += 1
        self.__signal_recu = value
        self.__logger.info("Signal recu")
