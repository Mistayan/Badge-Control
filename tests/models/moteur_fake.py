"""
Moteur

Un moteur est monté sur une porte et est relié à un lecteur de badge.
Quand le lecteur de badge est activé, il attend le scan du moteur.
Régulièrement, le moteur vérifie si les lecteurs de badges reliés ont été activés.
"""
import logging

from src.models.custom_error import CustomError
from src.models.ifaces.interface_lecteur_badge import ILecteurBadge
from src.models.ifaces.interface_moteur import IMotor


class Motor(IMotor):

    @property
    def id(self):
        return self.__id

    def __init__(self, lecteurs_badges, porte):
        super().__init__(lecteurs_badges, porte, control_center=None)
        self.__id = str(id(self))
        self._logger = logging.getLogger(self.__class__.__name__)
        self.__lecteurs_badges = lecteurs_badges
        self.__porte = porte

        # start watching __lecteur_badge.signal_ouverture
        # if the value changes, call __callback
        if not isinstance(self.__lecteurs_badges, list) or len(self.__lecteurs_badges) == 0:
            raise CustomError("Les lecteurs de badges sont requis")
        for lecteur in self.__lecteurs_badges:
            if not isinstance(lecteur, ILecteurBadge):
                raise CustomError(str(lecteur) + " n'est pas un lecteur de badge")
            lecteur.add_callback(self.__callback)

    def __callback(self, badge):
        """
        Callback du lecteur de badge.
        :param badge:
        """
        self._logger.info("Callback du lecteur de badge")
        if badge is not None:
            self._logger.info("Signal d'ouverture de la porte")
            self.__porte.signal_recu = True
        else:
            self._logger.info("Signal de fermeture de la porte")
            self.__porte.signal_recu = False
        return self.__porte.signal_recu
