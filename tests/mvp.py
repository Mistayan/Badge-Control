import logging
import unittest

import colorama

from src.models.badge import Badge
from src.models.centre_de_control import CentreDeControl
from src.models.custom_error import CustomError
from src.models.ifaces.interface_lecteur_badge import ILecteurBadge
from src.models.moteur import Motor
from tests.models.lecteur_badge_fake import LecteurBadgeFake
from tests.models.porte_spy import PorteSpy

colorama.init()
logging.basicConfig(level=logging.DEBUG)


def configure_test_environment(number_of_readers):
    porte = PorteSpy()
    control_center = CentreDeControl()
    lecteurs_badge = [LecteurBadgeFake() for _ in range(number_of_readers)]
    return (porte,
            control_center,
            lecteurs_badge[0] if len(lecteurs_badge) == 1 else lecteurs_badge,
            Motor(lecteurs_badges=lecteurs_badge, porte=porte, control_center=control_center))


class TestMVP(unittest.TestCase):

    def test_uc1(self):
        """ ### UC1: Ouverture de porte """
        # ETANT DONNE une Porte reliée à un Lecteur de badge
        # ET un Moteur d'Ouverture interrogeant ce Lecteur
        porte, control_center, lecteur_badge, moteur = configure_test_environment(1)

        # Et un badge valide, ayant les accès requis à la porte
        badge = control_center.register(Badge(), allowed_motor=moteur)

        # Quand ce badge est présenté sur le lecteur
        lecteur_badge.detecte_badge(badge)

        # Alors le signal d'ouverture de la porte est envoyé
        self.assertTrue(porte.ouverte)

    def test_uc2(self):
        """ ### UC2: Refus d'ouverture de porte, badge invalide """
        # Etant donné une porte reliée à un lecteur de badge
        # Et un moteur d'ouverture interrogeant ce lecteur
        porte, control_center, lecteur_badge, moteur = configure_test_environment(1)

        # Et un badge invalide (ayant ou non les accès)
        badge = control_center.register(Badge(), allowed_motor=moteur)
        control_center.block(badge)

        # Quand ce badge est présenté sur le lecteur
        lecteur_badge.detecte_badge(badge)

        # Alors le signal d'ouverture de la porte n'est pas envoyé et la porte reste fermée
        self.assertFalse(porte.ouverte)

    def test_uc3(self):
        """ ### UC3: Réactivation de badge """
        # Etant donné une porte reliée à un lecteur de badge
        # Et un moteur d'ouverture interrogeant ce lecteur
        porte, control_center, lecteur_badge, moteur = configure_test_environment(1)

        # Et un badge bloqué, ayant les accès requis à la porte, qui a été débloqué
        badge = control_center.register(Badge(), allowed_motor=moteur)
        control_center.block(badge)
        control_center.unblock(badge)

        # Quand ce badge est présenté sur le lecteur
        lecteur_badge.detecte_badge(badge)

        # Alors le signal d'ouverture de la porte est envoyé à la porte
        self.assertTrue(porte.ouverte)

    def test_uc4(self):
        """UC4: Lecteur défaillant"""
        #    ETANT DONNE un Lecteur défaillant relié à une Porte
        porte = PorteSpy()

        control_center = CentreDeControl()
        lecteur_badge: ILecteurBadge = None

        # Et un badge valide, ayant les accès requis à la porte
        with self.assertRaises(CustomError) as e:
            moteur = Motor(lecteurs_badges=[lecteur_badge], porte=porte, control_center=control_center)
            badge = control_center.register(Badge(), allowed_motor=moteur)

        # QUAND on interroge ce Lecteur
        with self.assertRaises(AttributeError):
            lecteur_badge.detect_badge(badge)

        # ALORS aucun signal d'ouverture n'est envoyé à la Porte
        self.assertFalse(porte.ouverte)

        # ET l'erreur est loguée
        self.assertEqual(str(e.exception), "None n'est pas un lecteur de badge")

    def test_uc5(self):
        """UC5 : Aucun badge présenté """
        # ETANT DONNE un Lecteur de badge relié à une Porte
        # ET un Moteur d'Ouverture interrogeant ce Lecteur
        porte, control_center, lecteur_badge, moteur = configure_test_environment(1)

        # QUAND aucun badge n'est présenté
        with self.assertRaises(TypeError):
            lecteur_badge.detecte_badge(None)

        # ALORS aucun signal d'ouverture n'est envoyé à la Porte
        self.assertFalse(porte.ouverte)

    def test_uc6(self):
        """ UC6: Ouverture de la porte défaillante (même que UC4 mais pour la porte)"""
        # ETANT DONNE une Porte défaillante reliée à un Lecteur
        porte = None
        control_center = CentreDeControl()
        lecteur_badge = LecteurBadgeFake()
        # ET un Moteur d'Ouverture interrogeant ce Lecteur
        # Et un badge valide, ayant les accès requis à la porte
        with self.assertRaises(CustomError) as e:
            moteur = Motor(lecteurs_badges=[lecteur_badge], porte=porte, control_center=control_center)

        with self.assertRaises(UnboundLocalError):
            badge = control_center.register(Badge(), allowed_motor=moteur)
            lecteur_badge.detecte_badge(badge)

        # ALORS aucun signal d'ouverture n'est envoyé à la Porte
        with self.assertRaises(AttributeError):
            self.assertFalse(porte.ouverte)

        # ET l'erreur est loguée
        self.assertEqual(str(e.exception), "La porte est requise")

    def test_uc7(self):
        """UC7 : Deux lecteurs reliés à une porte"""
        for i in range(2):  # Test each reader separately
            with self.subTest(reader=i + 1):
                porte, control_center, lecteurs_badge, moteur = configure_test_environment(2)

                # QUAND un Badge valide et ayant les accès requis à la Porte est présenté sur le Lecteur
                badge = control_center.register(Badge(), allowed_motor=moteur)
                lecteurs_badge[i].detecte_badge(badge)

                # ALORS un signal d'ouverture est envoyé à la Porte
                self.assertTrue(porte.ouverte)

    def test_uc8(self):
        """UC8: Une personne passe plusieurs fois son badge"""
        # ETANT DONNE une Porte reliée à un Lecteur
        # ET un Moteur d'Ouverture interrogeant ce Lecteur
        porte, control_center, lecteur_badge, moteur = configure_test_environment(1)

        # ET un individu ayant un badge valide, avec les accès requis à la porte
        badge = control_center.register(Badge(), allowed_motor=moteur)

        # QUAND il passe son badge devant le lecteur plusieurs fois (<N> fois)
        n = 3
        for _ in range(n):
            lecteur_badge.detecte_badge(badge)

        # ALORS il y a <N> impulsions envoyées
        self.assertEqual(n, porte.count)
        # ET la porte est ouverte
        self.assertTrue(porte.ouverte)

# ## END OF MVP ###
