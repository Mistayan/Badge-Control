# Badge-Control

_____

### Auteurs

- [Stephen proust](https://gitlab.com/Mistayan)
- [Olivier Bricaud](https://gitlab.com/Bob-117)
- [Aurelien Gouriou](https://gitlab.com/Sullfurick)

## Description

L'objectif de cet exercice-projet est de réaliser un système de contrôle d'accès par badge.

La méthodologie de développement abordé est EATDD (Example, Automate, Test-Driven, Domain-Driven).

## Prérequis

- Python 3.10+

## Installation

```shell
python setup.py
activate venv
```

# MVP (Minimum Viable Product):

______

## Cas nominaux

### UC1: Ouverture de porte

    ETANT DONNE une Porte reliée à un Lecteur
    ET un Moteur d'Ouverture interrogeant ce Lecteur
    QUAND un Badge valide est passé devant le Lecteur
    ALORS un signal d'ouverture est envoyé à la Porte

### UC2: Refus d'ouverture de porte, badge invalide

    ETANT DONNE une porte reliée à un lecteur
    ET un badge ayant été bloqué
    QUAND on présente ce badge à ce lecteur
    ALORS la porte ne reçoit pas de signal d'ouverture

### UC3: Réactivation de badge

    ETANT DONNE une porte reliée à un lecteur
    ET un badge ayant été bloqué puis débloqué
    QUAND on présente ce badge à ce lecteur
    ALORS la porte reçoit un signal d'ouverture

### UC4: Lecteur défaillant

    ETANT DONNE un Lecteur défaillant relié à une Porte
    QUAND on interroge ce Lecteur
    ALORS aucun signal d'ouverture n'est envoyé à la Porte
    ET l'erreur est loguée

### UC5: Aucun badge présenté

    ETANT DONNE une Porte reliée à un Lecteur
    ET un Moteur d'Ouverture interrogeant ce Lecteur
    QUAND aucun Badge valide n'est passé devant le Lecteur
    ALORS aucun signal d'ouverture n'est envoyé à la Porte

### UC6: Ouverture de la porte défaillante (même que UC4 mais pour la porte)

    ETANT DONNE une Porte défaillante reliée à un Lecteur
    ET un Moteur d'Ouverture interrogeant ce Lecteur
    QUAND un Badge valide est passé devant le Lecteur
    ALORS aucun signal d'ouverture n'est envoyé à la Porte
    ET l'erreur est loguée

### UC7: Deux lecteurs reliés à une porte

    ETANT DONNE une Porte reliée à deux Lecteurs
    ET un Moteur d'Ouverture interrogeant ces Lecteurs
    QUAND un Badge valide est passé devant un des Lecteurs
    ALORS un signal d'ouverture est envoyé à la Porte

### UC8: Une personne passe plusieurs fois son badge

    ETANT DONNE une Porte reliée à un Lecteur
    ET un Moteur d'Ouverture interrogeant ce Lecteur
    ET un individu possédant un Badge valide et autorisé
    QUAND il passe son badge devant le lecteur plusieurs fois (<N> fois ?)
    ALORS il y a <N> impulsions envoyées

### UCXX: Cas in-testables

    Le Moteur est bloqué : On ne peut pas tester ce cas car le moteur est un composant matériel
    La Porte est bloquée : On ne peut pas tester ce cas car la porte est un composant matériel
    Le Lecteur ne détecte pas le badge

#### time sequence

```mermaid
sequenceDiagram
    actor U as Employee
    participant BR as Badge reader
    participant MO as Motor
    participant D as Door
    participant CTRL as Control system
    actor CP as Control Person
    MO ->> BR: scan signal
    MO ->> MO: signal nok

    alt UC2: Refus d'ouverture de porte, badge invalide
        U ->> BR: Présente badge
        BR ->> CTRL: Validation badge
        CTRL ->> BR: NOK
        MO ->> BR: scan signal
        MO ->> MO: signal nok
        U ->> D: Ouvrir
        D ->> D: ne bouge pas
    end

    alt UC3: (Ré)Activation d'un badge
        CP ->> CTRL: login()
        CTRL ->> CP: logged
        CP ->> CTRL: activate(badge.id)
    end

    alt UC1: Ouverture de porte
        U ->> BR: Présente badge
        BR ->> CTRL: Validation badge
        CTRL ->> BR: OK
        BR ->> BR: signal actif pendant 3s
        MO ->> BR: scan signal
        MO ->> MO: signal ok
        MO ->> D: Signal Ouverture
        D ->> D: Ouvrir
        U ->> D: Ouvrir
        MO ->> BR: scan signal
        MO ->> MO: signal nok
        MO --> MO: attendre porte fermée
        U ->> U: Rentre
        D ->> D: Se ferme
        D ->> D: Fermer verrou
    end
```

## Iter 2

Le client souhaite faire évoluer son système pour permettre un contrôle d'accès plus fin.
Il souhaite que l'on refuse tout par défaut,
Si la personne a les droits d'accès à la porte badgée,
alors le signal d'ouverture est envoyé

nous pouvons donc commencer par remplacer

- "QUAND un Badge valide est passé devant le Lecteur" par ->
- "QUAND un Badge valide et autorisé sur cette porte est passé devant le Lecteur"

Notre diagramme de séquence ne change pas.
On changera légèrement notre lecteur de tests, afin de lui ajouter un ID, pour le centre de controle
On devra ajouter une option à notre centre de contrôle, pour gérer ces nouveaux droits.

## Définition des nouveaux cas de tests

### UC9: Une personne possède un badge qui ouvre plusieurs portes
    ETANT DONNE un centre de controle
    ET deux portes autorisée
    ET une porte non autorisée
    ET un individu ayant un badge valide, avec les accès requis à la porte pour les deux portes autorisées
    QUAND le badge est passe successivement devant les trois lecteurs
    ALORS les deux portes autorisées sont ouvertes avec le meme badge, et une porte non autorisée reste fermée

### UC10: Ajout d'autorisation d'accès d'un badge connu
    ETANT DONNE un centre de contrôle et un moteur
    QUAND on ajoute l'accès à un moteur pour un badge connu
    ALORS l'identifiant du moteur est ajouté à la liste d'accès du badge

### UC11: Ajout d'autorisation d'accès d'un badge inconnu
    ETANT DONNE un centre de contrôle et un moteur
    QUAND on tente d'autoriser l'accès à un moteur pour un badge qui n'existe pas
    ALORS une erreur est levée

### UC12: Retrait d'autorisation d'un badge connu
    ETANT DONNE un centre de contrôle, un moteur et un accès ajouté pour un badge
    QUAND on supprime l'accès à un moteur pour un badge connu 
    ALORS l'identifiant du moteur est supprimé de la liste d'accès du badge

### UC13: Retrait d'autorisation d'un badge inconnu
    ETANT DONNE un centre de contrôle et un moteur
    QUAND on supprime l'accès à un moteur pour un badge inconnu
    ALORS une erreur est levée

### UC14: Informations concernant un badge
    ETANT DONNE un badge
    QUAND on souhaite recupere des informations
    ALORS l'id et le status d'activation du badge sont données

### UC15: Refus d'ouverture de porte, le badge n'a pas l'accès

    ETANT DONNE une porte reliée à un lecteur
      ET un badge n'ayant pas les droits d'accès à la porte
    QUAND on présente ce badge à ce lecteur
    ALORS la porte ne reçoit pas de signal d'ouverture

### UC1(0): Ouverture de porte, le badge a l'accès

      ETANT DONNE une porte reliée à un lecteur
        ET un badge ayant les autorisations d'accès à la porte
      QUAND on présente ce badge à ce lecteur
      ALORS la porte reçoit un signal d'ouverture

#### Tests (run with coverage)

````
python -m pytest --cov --cov-report term --cov-report xml:coverage.xml tests/mvp.py

python -m pytest -vvvv --cov --cov-report html:coverage_html tests/mvp.py
````
